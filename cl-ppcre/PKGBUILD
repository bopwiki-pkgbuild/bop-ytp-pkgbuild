# Maintainer: Valenoern <valenoern@distributary>
# Contributor: Edi Weitz <edi@weitz.de>  (upstream creator)
pkgname=cl-ppcre  # 2.1.1-1 {{{
pkgver=2.1.1  # upstream version
pkgrel=1
arch=(any)
license=(BSD)
pkgdesc="Portable regular expression library for Common Lisp"
url=https://edicl.github.io/cl-ppcre/
depends=()  # no known dependencies to use this system

# duct tape for pkgbend  /  2022-01-18 "Simplify .asd and shut up modern ASDFs"
_repo="https://codeberg.org/bopwiki-pkgbuild/mirror-ppcre.git"
_commit="deb5f7cd09f0be1a2d41b4046e02b47123ac143c"
source=("${pkgname%-git}::git+${_repo}#commit=${_commit}")
sha1sums=('SKIP')
# }}}

# prepare package folder
# reference: https://packages.debian.org/bullseye/all/cl-ppcre/filelist
# BUG: one of the files' truenames is broken
package() {
 _systemname=${pkgname}
 _lispsource=usr/share/common-lisp/source/${_systemname}
 _lispsystems=usr/share/common-lisp/systems
 
 # install lisp files to /usr/share
 for file in ${srcdir}/${pkgname}/{*.lisp,*.asd}; do
  _filename=`basename $file`
  install -D -m644 "$file" "${pkgdir}/${_lispsource}/$_filename"
 done
 # install second system
 _dir=cl-ppcre-unicode
 #mkdir -p "${pkgdir}/${_lispsource}/${_dir}"
 for file in ${srcdir}/${pkgname}/${_dir}/{*.lisp,*.asd}; do
   if [[ -f $file ]] ; then
    _filename=`basename $file`
    install -D -m644 "$file" "${pkgdir}/${_lispsource}/${_dir}/$_filename"
   fi
 done
 
 # symlink system under systems directory
 mkdir -p "${pkgdir}/${_lispsystems}"
 ln -s "../source/${_systemname}/${_systemname}.asd" "${pkgdir}/${_lispsystems}/${_systemname}.asd"
 
 # documentation
 install -D -m644 "${srcdir}/${pkgname}/README.md" "${pkgdir}/usr/share/doc/${pkgname}/README.md"
 install -D -m644 "${srcdir}/${pkgname}/docs/index.html" "${pkgdir}/usr/share/doc/${pkgname}/index.html"
}
