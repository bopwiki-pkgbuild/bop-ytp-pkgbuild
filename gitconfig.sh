#!/bin/bash

git remote remove origin
git remote add cberg git@codeberg.org:bopwiki-pkgbuild/bop-ytp-pkgbuild.git
# this is an arbitrary bind mount I set up
git remote add arc /mnt/src-backup/pkg-bop-ytp.git

# end of file
