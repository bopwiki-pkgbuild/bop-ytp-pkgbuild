#!/bin/bash

export CONTENTS_PATH="./pkg_debian"
export PACKAGE_PATH="bop-owl-wings_2021.*_all.deb"

# run test using pkgbend
/usr/share/pkgbend/deb_test.sh
