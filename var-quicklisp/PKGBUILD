# Maintainer: Valenoern <valenoern@distributary>
# Contributor: George Rawlinson <grawlinson@archlinux.org> (Arch extra)
pkgname=quicklisp-var
pkgver=2023.02.15
pkgrel=1
_basepkg=quicklisp
arch=(any)
license=(MIT)
pkgdesc="A library manager for Common Lisp; v-refracta version, installs to /var"
url=https://www.quicklisp.org
depends=('common-lisp')
makedepends=('common-lisp')
provides=('quicklisp')
conflicts=('quicklisp')

source=(
	# in "extra" package
	'https://beta.quicklisp.org/quicklisp.lisp' #{,.asc}
	'LICENSE'
	'README.md'
	'FAQ.md'
	# new
	'quicklisp.conf'
)
b2sums=(
	'57d0d29e08d77176fea4f01e644fc11bdb96e9efbd132cbb9720192d3b47599f5ce3c6e0307b44c33add06bd70c28004b8efb8968f1d9deb881a2db4ac615772' # 'SKIP'
	'e7a64d5d86ea97cd694c9d72c602036003657b471c278e0116cc55aa72c817190446e542abf359f7424962b65bd33f4c9c8aa168de0870b9f9fc11e14801577f'
	'b01b32e0149a45ebcc1d9a6013b7e85733d219e65ba0545b1c4194e16ac0f5901317dc66eb1c437175b104f899a73a4af278d542c35e2d0ca37caadb99b4520c'
	'532cda6cdb5dd65ea0c091350f9fa2e6c3fa8abb6737179be495d6f3eafbda6b16253df666f51889aa76fb1c7fa4aecc9d7a499fcdcb62749f10e41cd9b21fff'
	# new
	'e08e2e526882ef1da88750fb4ea53eae77369502075e905005d6db279ecd9c94392117b9ef5a2e9915115509b9a67f52cbd7d4df7b413dae6e0b69b9f542f7cc'
)
#validpgpkeys=('D7A3489DDEFE32B7D0E7CC61307965AB028B5FF7') # release@quicklisp.org

# notes
# * package based on Arch "extra" repository - https://archlinux.org/packages/extra/any/quicklisp/
# * the quicklisp installer says "2015-01-28", but the real quicklisp dist inside the installer is "2023-02-15". I'm not totally sure how to get this
#pkgver() {
# grep 'defvar qlqs-info' quicklisp.lisp | sed -e 's/.*"\(.*\)".*/\1/' -e 's/-//g'
#}


# the normal Arch quicklisp package provides a quicklisp installer and then installs it again to /home. the installer may never be used after that
# I don't think this makes a lot of sense for a distribution which is going to be heavily focused on using Lisp applications as regular applications
# so instead, I decided to try installing quicklisp inside /var, and installing it only once instead of twice
# this has a couple of annoyances - you must set the permissions just right, and you need to make a user group for users to access the quicklisp directory

build() {
 mkdir -p "${srcdir}/var-lib-${_basepkg}"

 # unpack the quicklisp installer into an installed version
 _install_command="(quicklisp-quickstart:install :path \"${srcdir}/var-lib-${_basepkg}\")"
 if [[ -f /usr/bin/sbcl ]]; then
   sbcl --load 'quicklisp.lisp' --eval "$_install_command" --quit
 elif [[ -f /usr/bin/clisp ]]; then
   clisp -x '(load "quicklisp.lisp")' -x $_install_command
 elif [[ -f /usr/bin/ecl ]]; then
   ecl --load 'quicklisp.lisp' --eval $_install_command --eval '(quit)'
 fi

 # remove extraneous files
 rm ${srcdir}/var-lib-${_basepkg}/tmp/quicklisp.tar
 #rm -rf ${srcdir}/var-lib-${_basepkg}/local-projects

 # note: local-projects is the only part of the quicklisp folder that doesn't make sense as basically just another distribution package which happens to keep further packages inside it
 # for local projects, users can simply keep them somewhere in /home and add this path to a configuration file like ~/.sbclrc
}

package() {
 # I tried using `install` but it didn't want to work
 mkdir -p ${pkgdir}/var/lib/
 cp -r ${srcdir}/var-lib-${_basepkg} ${pkgdir}/var/lib/${_basepkg}
 chmod 664 ${pkgdir}/var/lib/${_basepkg} -R
 chmod 775 ${pkgdir}/var/lib/${_basepkg}/{,dists,quicklisp,tmp,local-projects} ${pkgdir}/var/lib/${_basepkg}/dists/quicklisp
 chgrp 1959 ${pkgdir}/var/lib/${_basepkg} -hR

 # "lisp" is created as gid 1958 and "quicklisp" as gid 1959, referencing the approximate year Lisp was invented. these may change in the future if they cause any problems
 #  groupadd -g 1958 lisp
 #  groupadd -g 1959 quicklisp
 # make sure to add your user with:  sudo usermod -aG quicklisp user
 # (I would add this as a post-install or something but I don't entirely know how)

 # create systemd config file for system user
 # https://man.archlinux.org/man/sysusers.d.5
 # note that only v-refracta is supposed to have systemd
 install -vDm644 -t "${pkgdir}/usr/lib/sysusers.d" quicklisp.conf

 # documentation
 install -vDm644 -t "${pkgdir}/usr/share/doc/${_basepkg}" ./*.md
 install -vDm644 -t "${pkgdir}/usr/share/licenses/${_basepkg}" LICENSE
}
