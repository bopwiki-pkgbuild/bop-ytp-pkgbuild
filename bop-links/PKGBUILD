# Maintainer: Valenoern <valenoern@distributary>
pkgname=bop-dev-links  # 2022.12.01-1 {{{
pkgver=2022.12.01  # bopwiki version based on
pkgrel=2  # PKGBUILD update
arch=(any)
license=(GPL)
pkgdesc="symlinks for installing development version of bopwiki"
url=https://codeberg.org/Valenoern/bop-ytp-pkgbuild

provides=('bopwiki=2022.12.01')
conflicts=('bopwiki')
# this package can be installed instead of the "bopwiki" package but not beside it

depends=(
	# bop developer's packages
	'bop-runlisp'  # requires sbcl (currently)
	'bop-owl-wings>=2022.3.09'
	
	'cl-html-template>=0.9.2'  # asd 0.9.2 ; for bop-render-html
	'cl-rdfxml>=0.9'  # asd 0.9 ; for bop-render
	
	# "deb" = version in debian/devuan package repositories; tested on devuan chimaera
		# debian bullseye = devuan chimaera; debian bookworm = devuan daedalus
	# "asd" = version of installed lisp asdf system (.asd), usually using quicklisp's copy as reference
	
	'cl-ppcre>=2.1.1'
		# asd 2.1.1 ; for bop-rules, bop-afraid-bluebird
		# deb bullseye "20190407.git1ca0cd9-2" = 2.1.1
	'cl-osicat'
		# asd "20211209-git" / fb87839984 ; for render-html, render-activitypub
		# deb bookworm "0.7.0+git20210913.2a431d0-2"
			# requires: cl-alexandria cl-babel cl-cffi cl-trivial-features libffi-dev libffi8 pkg-config
	'cl-flexi-streams>=1.0.19'
	'cl-puri'
		# asd "20201016-git" ; for bop-render
		# deb bullseye 1:1.5.7.2-1
	
	'cl-fiveam>=1.4.2'
	
	# BUG: these are just asd versions. update with corresponding arch-specific versions when testing on arch
	
	# NOTE:
	# these comments are very important for tracking versions of dependencies, but mostly irrelevant for "end users", so leave them out of the "bopwiki" pkgbuild or debian/control files and only use dependency names there. if "end users" are having bugs with dependencies they can refer back to this file.
)
optdepends=(
	# bop developer's packages
	'ytp>=2021.12.14'  # for bop-render-ytp
	
	'kate'  # currently called as editor, but not supposed to be required
)
# }}}

# package building functions {{{

build() {
 # manually link extra files into build environment,
 # as they don't need to be downloaded by the sources= routine
 ln -s ../pkg-files ${srcdir}/pkg-files
}

# prepare package folder
package() {
 _lispsource=usr/share/common-lisp/source
 _lispsystems=usr/share/common-lisp/systems
 _engine=usr/share/bopwiki
 
 # create lisp directories
 #mkdir -p "${pkgdir}/${_engine}"
 mkdir -p "${pkgdir}/${_lispsource}"
 mkdir -p "${pkgdir}/${_lispsystems}"
 
 # link system folders + .asd files (lisp code) - function below
 _systems
 _extensions  # 'optional' systems, mostly experimental
 
 # install "bop" command to /usr/bin/bop
 install -D -m755 "${srcdir}/pkg-files/bop" "${pkgdir}/usr/bin/bop"
 
 # final step:
 # - link bopwiki source dir to /usr/share/bopwiki, using ./bash-tools/bop-branch.sh
 # package does not do this currently.
}

_systems() {
 # uses variables set in "package"
 _sourcename="core"; _installedname="bopwiki"; _link_system
 _sourcename="core-cli"; _installedname="bopwiki-cli"; _link_system
 _sourcename="rules"; _installedname="bop-${_sourcename}"; _link_system
 _sourcename="render"; _installedname="bop-${_sourcename}"; _link_system
 _sourcename="tests"; _installedname="bopwiki-test"; _link_system
}

_extensions() {
 # publish groups extension
 _sourcename="render-ext-pubgroup"; _installedname="bop-render-publishgroup"; _link_system
 # html renderer
 _sourcename="render-html"; _installedname="bop-render-html"; _link_system
 # gemini renderer
 _sourcename="render-gmi"; _installedname="bop-render-gemini"; _link_system
 # atom feed renderer
 _sourcename="render-ext-atom"; _installedname="bop-render-atom"; _link_system
 # activitypub extension - not quite out of alpha
 _sourcename="render-ext-ap"; _installedname="bop-render-activitypub"; _link_system
 # simple gamebook renderer
 _sourcename="render-ext-twinned"; _installedname="bop-render-twinned"; _link_system
 # "ytp" renderer to generate stylesheets etc
 _sourcename="render-ext-ytp"; _installedname="bop-render-ytp"; _link_system
 # init.lisp shortcuts demo
 _sourcename="core-ext-shortcuts"; _installedname="bop-valenoern-shortcuts"; _link_system
 # "afraid bluebird" - an experiment that's likely to be replaced by a simpler entry pronouncer function
 _sourcename="core-ext-bluebird"; _installedname="bop-afraid-bluebird"; _link_system
}

_link_system() {  # put asdf system in standard linux directories
 ln -s "/${_engine}/${_sourcename}" "${pkgdir}/${_lispsource}/${_installedname}"
 ln -s "../source/${_installedname}/${_installedname}.asd" "${pkgdir}/${_lispsystems}/${_installedname}.asd"
}

# }}}
