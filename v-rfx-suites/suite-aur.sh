#!/bin/bash

parent=/opt/v-refracta
aur=${parent}/v-refracta-aur
arg=$2
repodir=$1
repo=`basename $repodir`
pkgdb=`readlink -f ${parent}/pkg-db`
packages=`vrfx-packages $repo aur`

# for every package in monorepo directory
for pkg in $packages
do
 if [[ -d ${aur}/${pkg} ]]; then   # if package has a directory
   file=`ls ${aur}/${pkg}/${pkg}-*.tar.zst 2>/dev/null`

   if [[ ! -n "$file" ]]; then
     pushd ${aur}/${pkg} >/dev/null
      makepkg PKGBUILD
     popd >/dev/null
   fi
   sudo cp -av $file -t ${parent}/$repo
   packagefiles="$packagefiles $file"
 fi
done

sudo pacman -U $packagefiles --dbonly --cachedir ${parent}/$repo --dbpath $pkgdb

if [[ "$arg" = "--db" ]]; then
 vrfx-db $repo
fi
