#!/bin/bash

usr=/usr/share/v-refracta/suites
etc=/etc/v-refracta/suites
var=/var/cache/v-refracta/suites

fix_suite () {
 echo "" | sudo tee "${var}/$suite/core.TRUE" 1>/dev/null
 echo "" | sudo tee "${var}/$suite/aur.TRUE" 1>/dev/null

 # suite=$1
 rosters=$2
 # echo "ROSTERS: $rosters"
 for roster in $rosters
 do
  fullpath="${var}/$suite/$roster"
  packages=`vrfx-packages $fullpath`

  # echo "SUITE: $fullpath"
  for pkg in $packages
  do
   corepkg=`pamac search -r --no-aur ${pkg}*`
   if [[ -n "$corepkg" ]]; then
     echo "$pkg" | sudo tee -a "${var}/${suite}/core.TRUE" 1>/dev/null
    else
     echo "$pkg" | sudo tee -a "${var}/${suite}/aur.TRUE" 1>/dev/null
   fi
  done
  sudo rm $fullpath
 done

 pushd "${var}/$suite/" 1>/dev/null
  sudo rm -rf $rosters
 popd 1>/dev/null

 sudo mv "${var}/${suite}/core.TRUE" "${var}/${suite}/core"
 sudo mv "${var}/${suite}/aur.TRUE"  "${var}/${suite}/aur"
}

collect_suites () {
 dir=$1
 suites=`ls ${dir} | tr "\n" " "`
 # echo "COLLECTING SUITES: $suites"
 for suite in $suites
 do
  fullpath="${dir}/${suite}"
  foundsuites="$foundsuites $fullpath"

  # if suite is a single file, tentatively assume it is a "core" list of non-AUR packages
  if [[ -f $fullpath ]]; then
    newpath=${suite}/core
   else
    newpath=
  fi
  # copy suite to cache unless it already exists
  # suites in /etc are given priority over suites in /usr to allow creating experimental variations of existing suites
  if [[ ! -e "${var}/${suite}" ]]; then
   sudo mkdir -p "${var}/${suite}"
   sudo cp -a $fullpath ${var}/${newpath}
  fi
 done
}

main () {
 # clear out existing suite cache
 sudo rm -rf   "${var}"
 sudo mkdir -p "${var}"
 foundsuites=
 # collect all suites into new suite cache
 collect_suites "${etc}"
 collect_suites "${usr}"

 # correctly sort packages inside each suite in case there are errors
 tmpsuites=`ls ${var} | tr "\n" " "`
 # echo "FIXING SUITES: $tmpsuites"
 for suite in $tmpsuites
 do
  suitefiles=`ls ${var}/$suite | tr "\n" " "`
  fix_suite "$suite" "$suitefiles"
 done

 echo "$foundsuites"
}

main
